#pragma checksum "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "dc47cfd2d2d7ff9810110ea1f3409e2567edf108"
// <auto-generated/>
#pragma warning disable 1591
namespace UiPro.Pages.Business.Lisitng
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using UiPro;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using UiPro.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using BlazorInputFile;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using Blazored.Toast;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "D:\Personal\Bitbucket\GBTBIndia\UiPro\_Imports.razor"
using Blazored.Toast.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
using UiPro.CommonComponents;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/Business")]
    public partial class Business : BusinessBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "pad-l-15");
            __builder.AddMarkupContent(2, "\r\n    ");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "row");
            __builder.AddMarkupContent(5, "\r\n        ");
            __builder.AddMarkupContent(6, "<div class=\"topLabel col-md-6 col-xs-12 text-center\"><h4>Business List</h4></div>\r\n        ");
            __builder.OpenElement(7, "div");
            __builder.AddAttribute(8, "class", "col-md-6 col-xs-12 text-right");
            __builder.AddMarkupContent(9, "\r\n            ");
            __builder.OpenElement(10, "a");
            __builder.AddAttribute(11, "class", "btn btn-warning1");
            __builder.AddMarkupContent(12, "\r\n                ");
            __builder.OpenElement(13, "span");
            __builder.AddAttribute(14, "class", "oi oi-caret-left");
            __builder.AddAttribute(15, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 9 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                         CallGoBack

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(16, " Back");
            __builder.CloseElement();
            __builder.AddMarkupContent(17, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(18, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(19, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(20, "\r\n");
            __builder.CloseElement();
            __builder.AddMarkupContent(21, "\r\n<hr>\r\n");
            __builder.OpenElement(22, "div");
            __builder.AddAttribute(23, "class", "form-check-inline");
            __builder.AddMarkupContent(24, "\r\n    ");
            __builder.OpenElement(25, "label");
            __builder.AddAttribute(26, "class", "form-check-label");
            __builder.AddMarkupContent(27, "\r\n        ");
            __builder.OpenElement(28, "input");
            __builder.AddAttribute(29, "type", "checkbox");
            __builder.AddAttribute(30, "class", "form-check-input");
            __builder.AddAttribute(31, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 17 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                   ShowAllClick

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(32, "checked", 
#nullable restore
#line 17 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                                           bIsShowAll

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(33, "Show All\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(34, "\r\n");
            __builder.CloseElement();
            __builder.AddMarkupContent(35, "\r\n");
            __builder.OpenElement(36, "div");
            __builder.AddAttribute(37, "class", "form-check-inline");
            __builder.AddMarkupContent(38, "\r\n    ");
            __builder.OpenElement(39, "label");
            __builder.AddAttribute(40, "class", "form-check-label");
            __builder.AddMarkupContent(41, "\r\n        ");
            __builder.OpenElement(42, "input");
            __builder.AddAttribute(43, "type", "checkbox");
            __builder.AddAttribute(44, "class", "form-check-input");
            __builder.AddAttribute(45, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 22 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                   ApprovedClick

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(46, "checked", 
#nullable restore
#line 22 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                                            bIsApproved

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(47, "Approved\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(48, "\r\n");
            __builder.CloseElement();
            __builder.AddMarkupContent(49, "\r\n");
            __builder.OpenElement(50, "div");
            __builder.AddAttribute(51, "class", "form-check-inline");
            __builder.AddMarkupContent(52, "\r\n    ");
            __builder.OpenElement(53, "label");
            __builder.AddAttribute(54, "class", "form-check-label");
            __builder.AddMarkupContent(55, "\r\n        ");
            __builder.OpenElement(56, "input");
            __builder.AddAttribute(57, "type", "checkbox");
            __builder.AddAttribute(58, "class", "form-check-input");
            __builder.AddAttribute(59, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 27 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                   FeaturedClick

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(60, "checked", 
#nullable restore
#line 27 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                                            bIsFeatured

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(61, "Featured\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(62, "\r\n");
            __builder.CloseElement();
            __builder.AddMarkupContent(63, "\r\n");
            __builder.OpenElement(64, "div");
            __builder.AddAttribute(65, "class", "form-check-inline");
            __builder.AddMarkupContent(66, "\r\n    ");
            __builder.OpenElement(67, "label");
            __builder.AddAttribute(68, "class", "form-check-label");
            __builder.AddMarkupContent(69, "\r\n        ");
            __builder.OpenElement(70, "input");
            __builder.AddAttribute(71, "type", "checkbox");
            __builder.AddAttribute(72, "class", "form-check-input");
            __builder.AddAttribute(73, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 32 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                   ActiveClick

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(74, "checked", 
#nullable restore
#line 32 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                                          bIsActive

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(75, "Active\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(76, "\r\n");
            __builder.CloseElement();
            __builder.AddMarkupContent(77, "\r\n");
            __builder.OpenElement(78, "table");
            __builder.AddAttribute(79, "class", "table table-bordered table-hover");
            __builder.AddMarkupContent(80, "\r\n    ");
            __builder.AddMarkupContent(81, @"<thead>
        <tr>
            <th>sr.no.</th>
            <th>Icon</th>
            <th>BusinessName</th>
            <th>Category</th>
            <th>Contact No</th>
            <th>Email Id</th>
            <th>Active</th>
            <th>Approved</th>
            <th>Featured</th>
            <th>GstNo</th>
            <th>LicenseNo</th>
            <th>Description</th>
        </tr>
    </thead>
");
#nullable restore
#line 52 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
     if (_BusinessList != null)
    {

#line default
#line hidden
#nullable disable
            __builder.AddContent(82, "        ");
            __builder.OpenElement(83, "tbody");
            __builder.AddMarkupContent(84, "\r\n");
#nullable restore
#line 55 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
              
                int i = 0;
            

#line default
#line hidden
#nullable disable
#nullable restore
#line 58 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
             foreach (var item in _BusinessList)
            {

#line default
#line hidden
#nullable disable
            __builder.AddContent(85, "                ");
            __builder.OpenElement(86, "tr");
            __builder.AddMarkupContent(87, "\r\n");
#nullable restore
#line 61 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                      i = i + 1;

#line default
#line hidden
#nullable disable
            __builder.AddContent(88, "                    ");
            __builder.OpenElement(89, "td");
            __builder.AddAttribute(90, "class", "td1");
            __builder.AddContent(91, 
#nullable restore
#line 62 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                     i.ToString()

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(92, "\r\n                    ");
            __builder.OpenElement(93, "td");
            __builder.AddAttribute(94, "class", "td1");
            __builder.OpenElement(95, "img");
            __builder.AddAttribute(96, "src", 
#nullable restore
#line 63 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                               item.Logo

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(97, "alt", 
#nullable restore
#line 63 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                item.BusinessName

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(98, "style", "height:30px;");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(99, "\r\n                    ");
            __builder.OpenElement(100, "td");
            __builder.AddContent(101, 
#nullable restore
#line 64 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                         item.BusinessName

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(102, "\r\n                    ");
            __builder.OpenElement(103, "td");
            __builder.AddContent(104, 
#nullable restore
#line 65 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                         item.CategoryName

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(105, "\r\n                    ");
            __builder.OpenElement(106, "td");
            __builder.AddContent(107, 
#nullable restore
#line 66 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                         item.ContactNo

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(108, "\r\n                    ");
            __builder.OpenElement(109, "td");
            __builder.AddContent(110, 
#nullable restore
#line 67 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                         item.EmailId

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(111, "\r\n");
#nullable restore
#line 68 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                      string clsIsActive = @item.IsActive == true ? "check-circle btn btn-success" : "times-circle btn btn-danger";

#line default
#line hidden
#nullable disable
            __builder.AddContent(112, "                    ");
            __builder.OpenElement(113, "td");
            __builder.AddAttribute(114, "class", "text-center");
            __builder.AddMarkupContent(115, "\r\n                        ");
            __builder.OpenComponent<UiPro.CommonComponents.Confirm>(116);
            __builder.AddAttribute(117, "strId", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 70 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                         item.strBusinessId

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(118, "bIsActive", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 70 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                         item.IsActive

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(119, "strUrl", "Business/SetActiveBusiness");
            __builder.AddAttribute(120, "strMessage", "Are you sure to ");
            __builder.AddAttribute(121, "elementName", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 70 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                                                                                                                        item.BusinessName

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(122, "\r\n                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(123, "\r\n");
#nullable restore
#line 73 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                      string clsIsApproved = @item.IsApproved == true ? "check-circle btn btn-success" : "times-circle btn btn-danger";

#line default
#line hidden
#nullable disable
            __builder.AddContent(124, "                    ");
            __builder.OpenElement(125, "td");
            __builder.AddAttribute(126, "class", "text-center");
            __builder.AddMarkupContent(127, "\r\n                        ");
            __builder.OpenComponent<UiPro.Pages.Business.Lisitng.BisApproval>(128);
            __builder.AddAttribute(129, "strId", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 75 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                             item.strBusinessId

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(130, "bIsActive", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 75 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                             item.IsApproved

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(131, "BusinessNameP", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 75 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                                                              item.BusinessName

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(132, "\r\n                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(133, "\r\n");
#nullable restore
#line 77 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                      string clsIsFeatured = @item.IsFeatured == true ? "check-circle btn btn-success" : "times-circle btn btn-danger";

#line default
#line hidden
#nullable disable
            __builder.AddContent(134, "                    ");
            __builder.OpenElement(135, "td");
            __builder.AddAttribute(136, "class", "text-center");
            __builder.AddMarkupContent(137, "\r\n                    ");
            __builder.OpenComponent<UiPro.Pages.Business.Lisitng.IsFeaturedChild>(138);
            __builder.AddAttribute(139, "strId", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 79 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                             item.strBusinessId

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(140, "bIsActive", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 79 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                             item.IsFeatured

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(141, "BusinessNameP", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 79 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                                                                                                              item.BusinessName

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(142, "\r\n                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(143, "\r\n                    ");
            __builder.OpenElement(144, "td");
            __builder.AddContent(145, 
#nullable restore
#line 81 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                         item.GstNo

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(146, "\r\n                    ");
            __builder.OpenElement(147, "td");
            __builder.AddContent(148, 
#nullable restore
#line 82 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                         item.LicenseNo

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(149, "\r\n                    ");
            __builder.OpenElement(150, "td");
            __builder.AddContent(151, 
#nullable restore
#line 83 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
                         item.strDescription

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(152, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(153, "\r\n");
#nullable restore
#line 85 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
            }

#line default
#line hidden
#nullable disable
            __builder.AddContent(154, "        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(155, "\r\n");
#nullable restore
#line 87 "D:\Personal\Bitbucket\GBTBIndia\UiPro\Pages\Business\Lisitng\Business.razor"
    }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
