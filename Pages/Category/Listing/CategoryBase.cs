﻿using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Models.Request;
using Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UiPro.Caller;
using UiPro.CommonService;

namespace UiPro.Pages.Category.Listing
{
    public class CategoryBase : ComponentBase
    {
        [Inject]
        private ICallers _ICaller { get; set; }
        [Inject]
        public IEnumerable<GetCategoryList> _CategoryList { get; set; }
        
        protected override async Task OnInitializedAsync()
        {
            await GetCategory();
        }
        public async Task GetCategory()
        {
            ApiResponse objApiResponse = new ApiResponse();
            objApiResponse = await _ICaller.GetCall("Category/GetCategory");
            if (objApiResponse != null)
            {
                if (objApiResponse.success)
                {
                    if (objApiResponse.success)
                    {
                        if (objApiResponse.data != null)
                        {
                            _CategoryList = JsonConvert.DeserializeObject<IEnumerable<GetCategoryList>>(objApiResponse.data.ToString());
                        }
                    }
                }
            }
            objApiResponse = null;
        }

        [Inject]
        public IJSRuntime _IJSRuntime { get; set; }
        protected void CallGoBack()
        {
            _IJSRuntime.InvokeAsync<bool>("GoBackJs");
        }
        [Inject]
        public IToastService toastService { get; set; }
        public async Task DeleteCategory(string categoryid)
        {
            bool confirmed = await _IJSRuntime.InvokeAsync<bool>("confirm", "Are you sure to detete category?");
            if (confirmed)
            {
                ApiResponse objApiResponse = new ApiResponse();
                GetCategoryDetailsRequest objGetCategoryDetailsRequest = new GetCategoryDetailsRequest();
                objGetCategoryDetailsRequest.strCategoryId = categoryid;
                objApiResponse = await _ICaller.PostCall("Category/DeleteCategory",objGetCategoryDetailsRequest);
                if (objApiResponse != null)
                {
                    if (objApiResponse.success)
                    {
                        if (objApiResponse.success)
                        {
                            toastService.ShowSuccess(objApiResponse.message);
                            await GetCategory();
                        }
                        else
                        {
                            toastService.ShowError(objApiResponse.message);
                        }
                    }
                }
                objApiResponse = null;
            }
        }
    }
}
