﻿using Blazored.Toast.Services;
using BlazorInputFile;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Models.Request;
using Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UiPro.Caller;
using UiPro.CommonService;
using static System.Net.Mime.MediaTypeNames;

namespace UiPro.Pages.Category.Details
{
    public class CategoryDetailsBase : ComponentBase
    {
        [Inject]
        private ICallers _ICaller { get; set; }
        [Inject]
        private ICommonServices _ICommonServices { get; set; }
        [Inject]
        public IToastService toastService { get; set; }
        [Parameter]
        public string CategoryID_p { get; set; }
        public AddCategoryRequest _AddCategoryRequest = new AddCategoryRequest();
        public string ImageName { get; set; }
        protected override async Task OnInitializedAsync()
        {
            if (!string.IsNullOrEmpty(CategoryID_p))
            {
                GetCategoryDetailsRequest objGetCategoryDetailsRequest = new GetCategoryDetailsRequest();
                objGetCategoryDetailsRequest.strCategoryId = CategoryID_p;
                var Response = await _ICaller.PostCall("Category/GetCategoryDetails", objGetCategoryDetailsRequest);
                if (Response != null)
                {
                    if (Response.success)
                    {
                        GetCategoryList objGetCategoryList = new GetCategoryList();
                        objGetCategoryList = JsonConvert.DeserializeObject<GetCategoryList>(Response.data.ToString());
                        _AddCategoryRequest.CategoryName = objGetCategoryList.CategoryName;
                        _AddCategoryRequest.CategoryDetail = objGetCategoryList.CategoryDetail;
                        _AddCategoryRequest.CategoryPic = objGetCategoryList.CategoryPic;
                        _AddCategoryRequest.IsActive = objGetCategoryList.IsActive;
                    }
                }
            }
        }
        public IFileListEntry file;
        public async Task HandleFileSelected(IFileListEntry[] files)
        {
            if (files != null)
            {
                file = files.FirstOrDefault();
                if (file.Type == "image/jpeg" || file.Type == "image/jpg" || file.Type == "image/png")
                {
                    if (file != null)
                    {
                        ImageName = file.Name;
                        var ms = new MemoryStream();
                        await file.Data.CopyToAsync(ms);
                        var reader = new StreamReader(file.Data);
                        string imageBase64Data = Convert.ToBase64String(ms.ToArray());
                        _AddCategoryRequest.CategoryPic = imageBase64Data;
                    }
                }
            }
        }
        public async Task HandleValidSubmit()
        {
            if (string.IsNullOrEmpty(CategoryID_p))
            {
                var Response = await _ICaller.PostCall("Category/AddCategory", _AddCategoryRequest);
                if (Response != null)
                {
                    if (Response.success)
                    {
                        toastService.ShowSuccess(Response.message);
                        _ICommonServices.PageNavigation("/Category");
                    }
                    else
                    {
                        toastService.ShowError(Response.message);
                    }
                }
            }
            else
            {
                EditCategoryRequest objEditCategoryRequest = new EditCategoryRequest();
                objEditCategoryRequest.strCategoryId = CategoryID_p;
                objEditCategoryRequest.CategoryName = _AddCategoryRequest.CategoryName;
                objEditCategoryRequest.CategoryDetail = _AddCategoryRequest.CategoryDetail;
                objEditCategoryRequest.CategoryPic = _AddCategoryRequest.CategoryPic;
                objEditCategoryRequest.IsActive = _AddCategoryRequest.IsActive;
                var Response = await _ICaller.PostCall("Category/EditCategory", objEditCategoryRequest);
                if (Response != null)
                {
                    if (Response.success)
                    {
                        toastService.ShowSuccess(Response.message);
                        _ICommonServices.PageNavigation("/Category");
                    }
                    else
                    {
                        toastService.ShowError(Response.message);
                    }
                }
            }
        }
        [Inject]
        public IJSRuntime _IJSRuntime { get; set; }
        protected void CallGoBack()
        {
            _IJSRuntime.InvokeAsync<bool>("GoBackJs");
        }
        
    }
}
