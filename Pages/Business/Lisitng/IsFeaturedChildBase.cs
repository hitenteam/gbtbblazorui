﻿using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;
using Models.Request;
using Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UiPro.Caller;
using UiPro.CommonService;

namespace UiPro.Pages.Business.Lisitng
{
    public class IsFeaturedChildBase : ComponentBase
    {

        public string strStateId { get; set; }

        [Inject]
        private ICallers _ICaller { get; set; }
        [Inject]
        public IToastService toastService { get; set; }
        [Parameter]
        public string strId { get; set; }

        [Parameter]
        public bool bIsActive { get; set; }

        [Parameter]
        public string BusinessNameP { get; set; }

        public bool ActiveOperation { get; set; }

        public Business_SetFeatured _Business_SetFeatured = new Business_SetFeatured();
        public bool IsShow { get; set; }
        public async Task SetActive()
        {
            ActiveOperation = !bIsActive;
            _Business_SetFeatured.FromDate = DateTime.Today;
            _Business_SetFeatured.ToDate = DateTime.Today.AddYears(1);
            IsShow = true;
            await BindState();
        }
        public IEnumerable<StateList> _StateList { get; set; }

        public async Task BindState()
        {
            try
            {
                ApiResponse StateResponse = new ApiResponse();
                StateResponse = await _ICaller.GetCall("Area/GetState");
                if (StateResponse != null)
                {
                    if (StateResponse.success)
                    {
                        _StateList = JsonConvert.DeserializeObject<IEnumerable<StateList>>(StateResponse.data.ToString());
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public IEnumerable<CityList> _CityList { get; set; }
        public async Task BindCity(ChangeEventArgs e)
        {
            strStateId = e.Value.ToString();
            await GetCityList(strStateId);
        }
        public async Task GetCityList(string states)
        {
            try
            {
                ApiResponse cityResponse = new ApiResponse();

                cityResponse = await _ICaller.PostCall("Area/GetCity", states);
                if (cityResponse != null)
                {
                    if (cityResponse.success)
                    {
                        _CityList = JsonConvert.DeserializeObject<IEnumerable<CityList>>(cityResponse.data.ToString());
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void CloseModal()
        {
            IsShow = false;
        }
        public async Task HandleValidSubmit()
        {
            ApiResponse objApiResponse = new ApiResponse();
            _Business_SetFeatured.BusinessId = strId;
            objApiResponse = await _ICaller.PostCall("Business/SetFeaturedBusiness", _Business_SetFeatured);
            if (objApiResponse != null)
            {
                if (objApiResponse.success)
                {
                    toastService.ShowSuccess(objApiResponse.message);
                    bIsActive = !bIsActive;
                }
                else
                {
                    toastService.ShowError(objApiResponse.message);
                }
            }
            else
            {
                toastService.ShowError("Operation failed");
            }
            objApiResponse = null;
            IsShow = false;
        }
        public async Task Removesubscription()
        {
            ApiResponse objApiResponse = new ApiResponse();
            objApiResponse = await _ICaller.PostCall("Business/RemoveFeaturedBusiness", strId);
            if (objApiResponse != null)
            {
                if (objApiResponse.success)
                {
                    toastService.ShowSuccess(objApiResponse.message);
                    bIsActive = !bIsActive;
                }
                else
                {
                    toastService.ShowError(objApiResponse.message);
                }
            }
            else
            {
                toastService.ShowError("Operation failed");
            }
            objApiResponse = null;
            IsShow = false;
        }

    }
}
