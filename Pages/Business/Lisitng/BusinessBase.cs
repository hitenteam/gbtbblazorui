﻿using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Models.Request;
using Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UiPro.Caller;
using UiPro.CommonService;

namespace UiPro.Pages.Business.Lisitng
{
    public class BusinessBase : ComponentBase
    {
        [Inject]
        private ICallers _ICaller { get; set; }
        [Inject]
        private ICommonServices _ICommonServices { get; set; }
        [Inject]
        public IToastService toastService { get; set; }

        public IEnumerable<Business_GetAll> _BusinessList { get; set; }
        public bool bIsApproved { get; set; }
        public bool bIsActive { get; set; }
        public bool bIsFeatured { get; set; }
        public bool bIsShowAll { get; set; }
        protected override async Task OnInitializedAsync()
        {
            bIsShowAll = true;
            bIsApproved = false;
            bIsActive = false;
            bIsFeatured = false;
            await GetBusiness();
        }
        public async Task ShowAllClick()
        {
            bIsShowAll = !bIsShowAll;
            await GetBusiness();
        }
        public async Task ApprovedClick()
        {
            bIsApproved = !bIsApproved;
            await GetBusiness();
        }
        public async Task FeaturedClick()
        {
            bIsFeatured = !bIsFeatured;
            await GetBusiness();
        }
        public async Task ActiveClick()
        {
            bIsActive = !bIsActive;
            await GetBusiness();
        }
        public async Task GetBusiness()
        {
            ApiResponse objApiResponse = new ApiResponse();
            GetBusinessListRequest objGetBusinessListRequest = new GetBusinessListRequest();
            objGetBusinessListRequest.CategoryId = "";
            objGetBusinessListRequest.IsApproved = bIsApproved;
            objGetBusinessListRequest.IsActive = bIsActive;
            objGetBusinessListRequest.IsFeatured = bIsFeatured;
            objGetBusinessListRequest.IsShowAll = bIsShowAll;
            objApiResponse = await _ICaller.PostCall("Business/GetBusinessList", objGetBusinessListRequest);
            if (objApiResponse != null)
            {
                if (objApiResponse.success)
                {
                    if (objApiResponse.data != null)
                    {
                        _BusinessList = JsonConvert.DeserializeObject<IEnumerable<Business_GetAll>>(objApiResponse.data.ToString());
                    }
                }
            }
            objApiResponse = null;
        }
       
     

        [Inject]
        public IJSRuntime _IJSRuntime { get; set; }
        protected void CallGoBack()
        {
            _IJSRuntime.InvokeAsync<bool>("GoBackJs");
        }

        public CommonComponents.ConfirmBase _ConfirmBase { get; set; }


    }
}
