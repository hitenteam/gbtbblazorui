﻿using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;
using Models.Request;
using Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UiPro.Caller;

namespace UiPro.Pages.Business.Lisitng
{
    public class BisApprovalBase : ComponentBase
    {
        [Inject]
        private ICallers _ICaller { get; set; }
        [Inject]
        public IToastService toastService { get; set; }
        [Parameter]
        public string strId { get; set; }

        [Parameter]
        public bool bIsActive { get; set; }

        [Parameter]
        public string BusinessNameP { get; set; }

        public bool ActiveOperation { get; set; }

        public Business_Approval _Business_Approval = new Business_Approval();
        public bool IsShow { get; set; }
        public void SetActive()
        {
            IsShow = true;
            _Business_Approval.FromDate = DateTime.Today;
            _Business_Approval.ToDate = DateTime.Today.AddYears(1);
            ActiveOperation = !bIsActive;
        }
        public void CloseModal()
        {
            IsShow = false;
        }
        public async Task HandleValidSubmit()
        {
            ApiResponse objApiResponse = new ApiResponse();
            _Business_Approval.BusinessId = strId;
            objApiResponse = await _ICaller.PostCall("Business/SetBusinessApproval", _Business_Approval);
            if (objApiResponse != null)
            {
                if (objApiResponse.success)
                {
                    toastService.ShowSuccess(objApiResponse.message);
                    bIsActive = !bIsActive;
                }
                else
                {
                    toastService.ShowError(objApiResponse.message);
                }
            }
            else
            {
                toastService.ShowError("Operation failed");
            }
            objApiResponse = null;
            IsShow = false;
        }
        public async Task Removesubscription()
        {
            ApiResponse objApiResponse = new ApiResponse();
            objApiResponse = await _ICaller.PostCall("Business/RemoveBusinessApproval", strId);
            if (objApiResponse != null)
            {
                if (objApiResponse.success)
                {
                    toastService.ShowSuccess(objApiResponse.message);
                    bIsActive = !bIsActive;
                }
                else
                {
                    toastService.ShowError(objApiResponse.message);
                }
            }
            else
            {
                toastService.ShowError("Operation failed");
            }
            objApiResponse = null;
            IsShow = false;
        }
    }
}
