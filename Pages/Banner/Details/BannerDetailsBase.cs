﻿using Blazored.Toast.Services;
using BlazorInputFile;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Models.Request;
using Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UiPro.Caller;
using UiPro.CommonService;

namespace UiPro.Pages.Banner.Details
{
    public class BannerDetailsBase : ComponentBase
    {
        [Inject]
        private ICallers _ICaller { get; set; }
        [Inject]
        private ICommonServices _ICommonServices { get; set; }
        [Inject]
        public IToastService toastService { get; set; }
        [Parameter]
        public string BannerId_p { get; set; }
        public string strStateId { get; set; }
        public AddNewBannerRequest _AddNewBannerRequest = new AddNewBannerRequest();
        public string ImageName { get; set; }
        protected override async Task OnInitializedAsync()
        {
            await BindState();
            await BindCategory();
            if (!string.IsNullOrEmpty(BannerId_p))
            {
                BannerDetailsRequest objBannerDetailsRequest = new BannerDetailsRequest();
                objBannerDetailsRequest.BannerId = BannerId_p;
                var Response = await _ICaller.PostCall("Banner/GetBannerDetails", objBannerDetailsRequest);
                if (Response != null)
                {
                    if (Response.success)
                    {
                        BannersDetails objBannersDetails = new BannersDetails();
                        objBannersDetails = JsonConvert.DeserializeObject<BannersDetails>(Response.data.ToString());
                        _AddNewBannerRequest.BannerName = objBannersDetails.BannerName;
                        strStateId = objBannersDetails.strStateId;
                        strCategory = objBannersDetails.strCategory;
                        await GetBusinessList(strCategory);
                        await GetCityList(strStateId);
                        _AddNewBannerRequest.CityId = objBannersDetails.strCityId;
                        _AddNewBannerRequest.BusinessId = objBannersDetails.strBusinessId;
                        _AddNewBannerRequest.FromDate = objBannersDetails.FromDate;
                        _AddNewBannerRequest.ToDate = objBannersDetails.ToDate;
                        _AddNewBannerRequest.IsActive = objBannersDetails.IsActive;
                        //_AddNewBannerRequest.IsPermenant = objBannersDetails.IsPermenant;
                        _AddNewBannerRequest.strImage = objBannersDetails.strImage;
                    }
                }
            }
            else
            {
                _AddNewBannerRequest.FromDate = DateTime.Now;
                _AddNewBannerRequest.ToDate = DateTime.Now.AddYears(1);
            }
        }
        public IFileListEntry file;
        public async Task HandleFileSelected(IFileListEntry[] files)
        {
            if (files != null)
            {
                file = files.FirstOrDefault();
                if (file.Type == "image/jpeg" || file.Type == "image/jpg" || file.Type == "image/png")
                {
                    if (file != null)
                    {
                        ImageName = file.Name;
                        var ms = new MemoryStream();
                        await file.Data.CopyToAsync(ms);
                        var reader = new StreamReader(file.Data);
                        string imageBase64Data = Convert.ToBase64String(ms.ToArray());
                        _AddNewBannerRequest.strImage = imageBase64Data;
                    }
                }
            }
        }
        public IEnumerable<StateList> _StateList { get; set; }
        public async Task BindState()
        {
            try
            {
                ApiResponse StateResponse = new ApiResponse();
                StateResponse = await _ICaller.GetCall("Area/GetState");
                if (StateResponse != null)
                {
                    if (StateResponse.success)
                    {
                        _StateList = JsonConvert.DeserializeObject<IEnumerable<StateList>>(StateResponse.data.ToString());
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public IEnumerable<CityList> _CityList { get; set; }
        public async Task BindCity(ChangeEventArgs e)
        {
            strStateId = e.Value.ToString();
            await GetCityList(strStateId);
        }
        public async Task GetCityList(string states)
        {
            try
            {
                ApiResponse cityResponse = new ApiResponse();

                cityResponse = await _ICaller.PostCall("Area/GetCity", states);
                if (cityResponse != null)
                {
                    if (cityResponse.success)
                    {
                        _CityList = JsonConvert.DeserializeObject<IEnumerable<CityList>>(cityResponse.data.ToString());
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public IEnumerable<GetCategoryList> _GetCategoryList { get; set; }
        public string strCategory { get; set; }
        public async Task BindCategory()
        {
            try
            {
                ApiResponse CategoryResponse = new ApiResponse();
                CategoryResponse = await _ICaller.GetCall("Category/GetCategory");
                if (CategoryResponse != null)
                {
                    if (CategoryResponse.success)
                    {
                        _GetCategoryList = JsonConvert.DeserializeObject<IEnumerable<GetCategoryList>>(CategoryResponse.data.ToString());
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public IEnumerable<Business_DropdownList> _Business_DropdownList { get; set; }
        public async Task BindBusiness(ChangeEventArgs e)
        {
            strCategory = e.Value.ToString();
            await GetBusinessList(strCategory);
        }
        public async Task GetBusinessList(string Categories)
        {
            try
            {
                ApiResponse BusinessResponse = new ApiResponse();
                BusinessResponse = await _ICaller.PostCall("Business/GetBusinessddl", Categories);
                if (BusinessResponse != null)
                {
                    if (BusinessResponse.success)
                    {
                        _Business_DropdownList = JsonConvert.DeserializeObject<IEnumerable<Business_DropdownList>>(BusinessResponse.data.ToString());
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public async Task HandleValidSubmit()
        {
            if (string.IsNullOrEmpty(BannerId_p))
            {
                var Response = await _ICaller.PostCall("Banner/AddNewBanner", _AddNewBannerRequest);
                if (Response != null)
                {
                    if (Response.success)
                    {
                        toastService.ShowSuccess(Response.message);
                        _ICommonServices.PageNavigation("/Banner");
                    }
                    else
                    {
                        toastService.ShowError(Response.message);
                    }
                }
            }
            else
            {
                EditBannerRequest objEditBannerRequest = new EditBannerRequest();
                objEditBannerRequest.BannerID = BannerId_p;
                objEditBannerRequest.BannerName = _AddNewBannerRequest.BannerName;
                objEditBannerRequest.BusinessId = _AddNewBannerRequest.BusinessId;
                objEditBannerRequest.CityId = _AddNewBannerRequest.CityId;
                objEditBannerRequest.FromDate = _AddNewBannerRequest.FromDate;
                objEditBannerRequest.ToDate = _AddNewBannerRequest.ToDate;
                objEditBannerRequest.IsActive = _AddNewBannerRequest.IsActive;
                objEditBannerRequest.strImage = _AddNewBannerRequest.strImage;
                var Response = await _ICaller.PostCall("Banner/EditBanner", objEditBannerRequest);
                if (Response != null)
                {
                    if (Response.success)
                    {
                        toastService.ShowSuccess(Response.message);
                        _ICommonServices.PageNavigation("/Banner");
                    }
                    else
                    {
                        toastService.ShowError(Response.message);
                    }
                }
            }
        }
        [Inject]
        public IJSRuntime _IJSRuntime { get; set; }
        protected void CallGoBack()
        {
            _IJSRuntime.InvokeAsync<bool>("GoBackJs");
        }
    }
}
