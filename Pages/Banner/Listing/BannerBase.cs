﻿using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Models.Request;
using Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UiPro.Caller;

namespace UiPro.Pages.Banner.Listing
{
    public class BannerBase : ComponentBase
    {
        [Inject]
        private ICallers _ICaller { get; set; }
        [Inject]
        public IEnumerable<GetBanner> _BannersDetails { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await GetBanners();
        }
        public async Task GetBanners()
        {
            ApiResponse objApiResponse = new ApiResponse();
            GetAllBannerRequest objGetAllBannerRequest = new GetAllBannerRequest();
            objGetAllBannerRequest.BannerType = BannerType;
            objApiResponse = await _ICaller.PostCall("Banner/GetAllBanner", objGetAllBannerRequest);
            if (objApiResponse != null)
            {
                if (objApiResponse.success)
                {
                    if (objApiResponse.data != null)
                    {
                        _BannersDetails = JsonConvert.DeserializeObject<IEnumerable<GetBanner>>(objApiResponse.data.ToString());
                    }
                }
            }
            objApiResponse = null;
        }
        public int BannerType { get; set; } = 0;
        public void BannerTypeSelection(ChangeEventArgs e)
        {
            BannerType = Convert.ToInt32(e.Value.ToString());
        }
        [Inject]
        public IJSRuntime _IJSRuntime { get; set; }
        protected void CallGoBack()
        {
            _IJSRuntime.InvokeAsync<bool>("GoBackJs");
        }
        [Inject]
        public IToastService toastService { get; set; }
        public async Task DeleteBanner(string categoryid)
        {
            bool confirmed = await _IJSRuntime.InvokeAsync<bool>("confirm", "Are you sure to detete banner?");
            if (confirmed)
            {
                ApiResponse objApiResponse = new ApiResponse();
                GetCategoryDetailsRequest objGetCategoryDetailsRequest = new GetCategoryDetailsRequest();
                objGetCategoryDetailsRequest.strCategoryId = categoryid;
                objApiResponse = await _ICaller.PostCall("/DeleteBanner", objGetCategoryDetailsRequest);
                if (objApiResponse != null)
                {
                    if (objApiResponse.success)
                    {
                            toastService.ShowSuccess(objApiResponse.message);
                            await GetBanners();
                    }
                    else
                    {
                        toastService.ShowError(objApiResponse.message);
                    }
                }
                objApiResponse = null;
            }
        }
    }
}