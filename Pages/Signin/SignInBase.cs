﻿using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;
using Models.Request;
using Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UiPro.CommonService;
using UiPro.Pages.Signin.SignInService;

namespace UiPro.Pages.Signin
{
    public class SignInBase : ComponentBase
    {
        [Inject]
        public ISignInServices _ISignInServices { get; set; }
        [Inject]
        public ICommonServices _ICommonServices { get; set; }
        public Login _Login = new Login();
        public bool isremember { get; set; }
        [Inject]
        public IToastService toastService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            if (!string.IsNullOrEmpty(await _ICommonServices.GetLocalStorage("Token")))
            {
                await _ICommonServices.RemoveLocalStorage("Token");
            }
        }
        public async Task checkUser()
        {
            TokenResponse _TokenResponse = new TokenResponse();
            var Response = await _ISignInServices.LoginCall(_Login);
            if (Response != null)
            {
                if (Response.success)
                {
                    try
                    {
                        _TokenResponse = JsonConvert.DeserializeObject<TokenResponse>(Response.data.ToString());
                        if (_TokenResponse.Token != null && _TokenResponse.UserType == 1)
                        {
                            await _ICommonServices.SetLocalStorage("Token", _TokenResponse.Token);
                            toastService.ShowSuccess("Login Succeed.");
                            _ICommonServices.PageNavigation("/");
                        }
                        else
                        {
                            toastService.ShowError("Invalid mobileno or passoword");
                        }
                    }
                    catch (Exception ex)
                    {
                        toastService.ShowError("Login failed");
                    }
                }
                else
                {
                    toastService.ShowError("Invalid mobileno or passoword");
                }
            }
            else
            {
                toastService.ShowError("Login failed");
            }
        }
    }
}
