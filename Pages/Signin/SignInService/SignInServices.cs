﻿using Models.Request;
using Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UiPro.Caller;

namespace UiPro.Pages.Signin.SignInService
{
    public class SignInServices : ISignInServices
    {
        private readonly ICallers _ICaller;
        public SignInServices(ICallers Caller)
        {
            _ICaller = Caller;
        }
        public async Task<ApiResponse> LoginCall(Login obj)
        {
            return await _ICaller.LoginCall("Account/login", obj);
        }
    }
}
