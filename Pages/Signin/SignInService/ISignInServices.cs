﻿using Models.Request;
using Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UiPro.Pages.Signin.SignInService
{
    public interface ISignInServices
    {
        Task<ApiResponse> LoginCall(Login obj);
    }
}
