﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UiPro.CommonService
{
    public interface ICommonServices
    {
        void PageNavigation(string strUrl);
        string CurrentPage();
        Task SetLocalStorage(string strKey, object strValue);
        Task RemoveLocalStorage(string strKey);
        Task<string> GetLocalStorage(string strKey);
    }
}
