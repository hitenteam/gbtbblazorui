﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace UiPro.CommonService
{
    public class CommonServices : ICommonServices
    {
        private readonly NavigationManager _NavigationManager;
        private readonly ILocalStorageService _localStore;
        private readonly ILoggerFactory _ILoggerFactory;
        public CommonServices(NavigationManager navigationManager, ILocalStorageService LocalStorageService,ILoggerFactory LoggerFactory)
        {
            _NavigationManager = navigationManager;
            _localStore = LocalStorageService;
            _ILoggerFactory = LoggerFactory;
        }
        public void PageNavigation(string strUrl)
        {
            try
            {
                _NavigationManager.NavigateTo(strUrl);
            }
            catch(Exception ex)
            {

            }
        }
        public string CurrentPage()
        {
            try
            {
               return _NavigationManager.Uri;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public async Task SetLocalStorage(string strKey,object strValue)
        {
            try
            {
                await _localStore.SetItemAsync(strKey, strValue);
            }
            catch(Exception ex)
            {

            }
        }
        public async Task RemoveLocalStorage(string strKey)
        {
            try
            {
                await _localStore.RemoveItemAsync(strKey);
            }
            catch(Exception ex)
            {

            }
        }
        public async Task<string> GetLocalStorage(string strKey)
        {
            try
            {
                return await _localStore.GetItemAsync<string>(strKey);
            }
            catch(Exception ex)
            {
                return null;
            }
        }

       
    }
}
