﻿using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;
using Models.Request;
using Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UiPro.Caller;

namespace UiPro.CommonComponents
{
    public class ConfirmBase : ComponentBase
    {
        [Inject]
        private ICallers _ICaller { get; set; }
        [Inject]
        public IToastService toastService { get; set; }
        [Parameter]
        public bool bIsActive { get; set; }
        [Parameter]
        public string strUrl { get; set; }
        [Parameter]
        public string strId { get; set; }

        [Parameter]
        public string strMessage { get; set; }
        [Parameter]
        public string elementName { get; set; }

        public bool IsShow { get; set; }
        public void SetActive()
        {
            IsShow = true;
        }
        public void CloseModal()
        {
            IsShow = false;
        }
        public async Task YesConfirm()
        {
            ApiResponse objApiResponse = new ApiResponse();
            ActiveDeactiveRequest objRequest = new ActiveDeactiveRequest();
            objRequest.strId = strId;
            objRequest.isActive = !bIsActive;
            objApiResponse = await _ICaller.PostCall(strUrl, objRequest);
            if (objApiResponse != null)
            {
                if (objApiResponse.success)
                {
                    toastService.ShowSuccess(objApiResponse.message);
                    bIsActive = !bIsActive;
                }
                else
                {
                    toastService.ShowError(objApiResponse.message);
                }
            }
            else
            {
                toastService.ShowError("Operation failed");
            }
            objApiResponse = null;
            IsShow = false;
            
        }
    }
}
