﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UiPro.Caller;
using UiPro.CommonService;
using UiPro.Pages.Signin.SignInService;

namespace UiPro
{
    public static class DIContainerExtension
    {
        public static void ConfigureDI(this IServiceCollection services, IConfiguration configuration)
        {
            string ApiUrl = configuration["ApiUrl"];
            services.AddHttpClient<ICallers, Callers>(client =>
            {
                client.BaseAddress = new Uri(ApiUrl);
            });
            services.AddTransient<ISignInServices, SignInServices>();
            services.AddTransient<ICommonServices, CommonServices>();
        }
    }
}
