﻿using Microsoft.AspNetCore.Components;
using Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using UiPro.CommonService;

namespace UiPro.Caller
{
    public class Callers : ICallers
    {
        private readonly HttpClient httpClient;
        private readonly ICommonServices _ICommonServices;
        public Callers(HttpClient httpClient, ICommonServices CommonServices)
        {
            this.httpClient = httpClient;
            _ICommonServices = CommonServices;
        }

        public async Task<ApiResponse> GetCall(string ApiEndPoint)
        {
            if (await ValidateToken())
            {
                string Token = await _ICommonServices.GetLocalStorage("Token");
                if (!string.IsNullOrEmpty(Token))
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                    return await httpClient.GetJsonAsync<ApiResponse>(ApiEndPoint);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                _ICommonServices.PageNavigation("/signin");
                return null;
            }
        }


        public async Task<ApiResponse> PostCall(string ApiEndPoint, object Param)
        {
            if (await ValidateToken())
            {
                string Token = await _ICommonServices.GetLocalStorage("Token");
                if (!string.IsNullOrEmpty(Token))
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                    return await httpClient.PostJsonAsync<ApiResponse>(ApiEndPoint, Param);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                _ICommonServices.PageNavigation("/signin");
                return null;
            }
        }
        public async Task<ApiResponse> LoginCall(string ApiEndPoint, object Param)
        {
            return await httpClient.PostJsonAsync<ApiResponse>(ApiEndPoint, Param);
        }

        public async Task<bool> ValidateToken()
        {
            try
            {
                string Token = await _ICommonServices.GetLocalStorage("Token");
                if (!string.IsNullOrEmpty(Token))
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                    var Result = await httpClient.GetJsonAsync<object>("Account/ValidatToken");
                    return bool.Parse(Result.ToString());
                }
                else
                {
                    return false;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
