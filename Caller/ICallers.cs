﻿using Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UiPro.Caller
{
    public interface ICallers
    {
        Task<ApiResponse> GetCall(string ApiEndPoint);
        Task<ApiResponse> PostCall(string ApiEndPoint, object Param);
        Task<ApiResponse> LoginCall(string ApiEndPoint, object Param);
        Task<bool> ValidateToken();
    }
}
